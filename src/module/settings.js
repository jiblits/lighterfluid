// SPDX-FileCopyrightText: 2021 Chris Saunders <chris@christophersaunders.ca>
//
// SPDX-License-Identifier: GPL-3.0-or-later

export function registerSettings() {
  const DEFAULT_LIGHTS = [
    { name: 'Torch', color: '#ff0000', alpha: 0.5, brightRadius: 20, dimRadius: 40 },
    { name: 'Light Spell', color: '#00ff00', alpha: 0.35, brightRadius: 20, dimRadius: 40 },
  ];

  game.settings.register('lighterfluid', 'lights', {
    name: 'Lights',
    scope: 'world',
    config: false,
    type: String,
    default: JSON.stringify(DEFAULT_LIGHTS),
    onChange: (value) => {
      console.log(value);
    },
  });
}
