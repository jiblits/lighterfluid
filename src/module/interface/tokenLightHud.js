// SPDX-FileCopyrightText: 2021 Chris Saunders <chris@christophersaunders.ca>
//
// SPDX-License-Identifier: GPL-3.0-or-later

import { LightSourceCollection } from '../lighting/lightSourceCollection';

export function registerTokenHUDControls(application) {
  class TokenLightHUD extends TokenHUD {
    constructor() {
      super();
      this.buildLightCollection();
      application.subscribe(application.constructor.EVENTS.SettingsSaved, () => {
        this.buildLightCollection();
      });
    }

    static get defaultOptions() {
      return mergeObject(super.defaultOptions, {
        id: TokenLightHUD.ID,
        template: 'modules/lighterfluid/templates/light-hud.hbs',
      });
    }

    buildLightCollection() {
      this.lightCollection = LightSourceCollection.build(application.config());
    }

    setPosition() {
      this.element.css({
        width: 100,
        left: 150,
        top: 0,
      });
    }

    getData() {
      return {
        lightSources: this.lightCollection.all(),
        id: this.id,
      };
    }

    activateListeners(html) {
      application.hudNode().append(html);
      html.on('click', '[data-action]', (event) => {
        console.log('Clicky click click ', event);
        console.log('Token info', this.token);
        this._handleButtonClick(event);
      });
    }

    bind(placeableObject) {
      super.bind(placeableObject);
      this.token = placeableObject;
    }

    async _handleButtonClick(event) {
      const clickedElement = event.currentTarget;
      const lightSourceId = clickedElement.dataset.action;
      const light = this.lightCollection.findById(lightSourceId);
      light.applyTo(this.token);
    }
  }

  TokenLightHUD.ID = 'token-light-hud';

  var lightHud = new TokenLightHUD();

  Hooks.on('controlToken', (placeableObject, shouldRender) => {
    if (shouldRender == false) {
      return;
    }

    lightHud.bind(placeableObject);
  });

  Hooks.on('renderTokenHUD', () => {
    lightHud.render();
  });
}
