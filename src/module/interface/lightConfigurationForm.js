// SPDX-FileCopyrightText: 2021 Chris Saunders <chris@christophersaunders.ca>
//
// SPDX-License-Identifier: GPL-3.0-or-later

import { LightSource } from '../lighting/lightSource';
import { LightSourceCollection } from '../lighting/lightSourceCollection';

export function registerSceneControls(application) {
  class LightConfigurationForm extends FormApplication {
    constructor() {
      super();
      this.lightCollection = LightSourceCollection.build(application.config());
    }

    static get defaultOptions() {
      return mergeObject(super.defaultOptions, {
        classes: ['form'],
        popOut: true,
        template: 'modules/lighterfluid/templates/light-form.hbs',
        id: LightConfigurationForm.FORM_ID,
        title: 'LighterFluid | Light Configuration',
        resizable: false,
        submitOnChange: true,
        closeOnSubmit: false,
      });
    }

    getData() {
      return {
        lightSources: this.lightCollection.all(),
        newLightSource: new LightSource(),
      };
    }

    addNewLightSource() {
      this.lightCollection.add(new LightSource());
      this.render();
    }

    findLightSource(lightSourceIdentifier) {
      return this.lightCollection.findById(lightSourceIdentifier);
    }

    removeLightSource(lightSourceIdentifier) {
      const lightSourceName = document.getElementById(lightSourceIdentifier).querySelector('.light-source-name').value;
      Dialog.confirm({
        title: `Remove ${lightSourceName}?`,
        content: 'Are you sure you want to remove this lighting configuration?',
        yes: () => {
          this.lightCollection.remove(lightSourceIdentifier);
          this.render();
        },
        no: () => {},
        defaultYes: false,
      });
    }

    activateListeners(html) {
      super.activateListeners(html);
      html.on('click', '[data-action]', (event) => {
        this._handleButtonClick(event);
      });
      this._recalculateOverlayHeight();
    }

    async _handleButtonClick(event) {
      const clickedElement = event.currentTarget;
      const action = clickedElement.dataset.action;
      const lightSourceId = clickedElement.dataset.identifier;
      switch (action) {
        case 'add':
          this.addNewLightSource();
          break;
        case 'delete':
          this.removeLightSource(lightSourceId);
          break;
      }
    }

    async _updateObject(event) {
      if (event.type == 'submit') {
        application.update('lights', this.lightCollection.all());
        return;
      }
      const element = event.target;
      var lightSource = this.findLightSource(element.dataset.identifier);
      lightSource[element.name] = element.value;
    }

    _recalculateOverlayHeight() {
      if (this.overlay == undefined) {
        this.overlay = window.document.getElementById(LightConfigurationForm.FORM_ID);
      }
      var totalHeight = 0;
      this.overlay.childNodes.forEach((node) => {
        totalHeight += node.scrollHeight || 0;
      });
      this.overlay.style.height = `${Math.min(totalHeight, LightConfigurationForm.MAX_HEIGHT)}px`;
    }
  }

  LightConfigurationForm.MAX_HEIGHT = 350;
  LightConfigurationForm.FORM_ID = 'lighterfluid-light-configuration';

  Hooks.on('getSceneControlButtons', (buttons) => {
    let lightingButtons = buttons.find((collection) => collection.name == 'lighting');
    lightingButtons.tools.push({
      name: 'lights',
      title: 'Lighting Presets',
      icon: 'fas fa-cloud-moon',
      onClick: () => {
        new LightConfigurationForm().render(true);
      },
    });
  });
}
