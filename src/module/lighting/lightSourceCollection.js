// SPDX-FileCopyrightText: 2021 Chris Saunders <chris@christophersaunders.ca>
//
// SPDX-License-Identifier: GPL-3.0-or-later

import { LightSource } from './lightSource';

export class LightSourceCollection {
  static build(appConfig) {
    return new LightSourceCollection(appConfig.lights.map((light) => new LightSource(light)));
  }

  constructor(lightSources) {
    this.lightSources = lightSources == undefined ? [] : lightSources;
  }

  all() {
    return this.lightSources;
  }

  add(lightSource) {
    this.lightSources.unshift(lightSource);
  }

  remove(lightSourceId) {
    this.lightSources = this.lightSources.filter((light) => light.identifier != lightSourceId);
  }

  findById(lightSourceId) {
    return this.lightSources.find((lightSource) => lightSource.identifier == lightSourceId);
  }
}
