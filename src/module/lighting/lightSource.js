// SPDX-FileCopyrightText: 2021 Chris Saunders <chris@christophersaunders.ca>
//
// SPDX-License-Identifier: GPL-3.0-or-later

export class LightSource {
  constructor(settings) {
    this.identifier = settings?.identifier;
    this.name = settings?.name;
    this.color = settings?.color;
    this.alpha = settings?.alpha;
    this.brightRadius = settings?.brightRadius;
    this.dimRadius = settings?.dimRadius;
    this.applyDefaults();
  }

  applyDefaults() {
    if (this.identifier == undefined) {
      this.identifier = randomID(32);
    }
  }

  applyTo(token) {
    token.update({
      lightAlpha: parseFloat(this.alpha),
      lightColor: this.color,
      dimLight: this.dimRadius,
      brightLight: this.brightRadius,
    });
    token.updateSource(false);
  }
}
