// SPDX-FileCopyrightText: 2021 Chris Saunders <chris@christophersaunders.ca>
//
// SPDX-License-Identifier: GPL-3.0-or-later

export async function preloadTemplates() {
  const templatePaths = [
    'modules/lighterfluid/templates/light-form.hbs',
    // Add paths to "modules/lighterfluid/templates"
  ];

  return loadTemplates(templatePaths);
}
