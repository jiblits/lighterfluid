// SPDX-FileCopyrightText: 2021 Chris Saunders <chris@christophersaunders.ca>
//
// SPDX-License-Identifier: GPL-3.0-or-later

import { registerSceneControls } from './interface/lightConfigurationForm.js';
import { registerTokenHUDControls } from './interface/tokenLightHud.js';

export class LighterFluid extends Application {
  constructor() {
    super();
    this.messageBus = LighterFluid.permittedEventListeners.reduce((accum, key) => {
      accum[key] = [];
      return accum;
    }, {});
  }

  config() {
    if (this.configuration == undefined) {
      this.configuration = {
        lights: JSON.parse(game.settings.get('lighterfluid', 'lights')),
      };
    }
    return this.configuration;
  }

  hudNode() {
    return game.canvas.hud.token.element;
  }

  update(key, value) {
    if (this.configuration[key] == undefined) {
      throw `Configuration key '${key}' has not been defined previously.`;
    }

    this.configuration[key] = value;
    this._persistSettings();
  }

  _persistSettings() {
    for (const [key, value] of Object.entries(this.configuration)) {
      game.settings.set('lighterfluid', key, JSON.stringify(value));
    }
    this._emit(LighterFluid.EVENTS.SettingsSaved);
  }

  static get permittedEventListeners() {
    return Object.values(LighterFluid.EVENTS);
  }

  subscribe(message, fn) {
    if (LighterFluid.permittedEventListeners.includes(message) == false) {
      throw `Cannot subscribe to events called ${message}. Emitted events are ${this.permittedEventListeners()}`;
    }

    this.messageBus[message].push(fn);
  }

  unsubscribe(message, fn) {
    this.messageBus[message] = this.messageBus[message].filter((subscriber) => subscriber != fn);
  }

  _emit(message) {
    const eventListeners = this.messageBus[message];
    if (eventListeners == undefined) {
      return;
    }

    eventListeners.forEach((listener) => listener());
  }

  registerHooks() {
    console.log('Registering hooks');
    registerSceneControls(this);
    registerTokenHUDControls(this);
  }
}

LighterFluid.EVENTS = {};
LighterFluid.EVENTS.SettingsSaved = 'settingsSaved';
