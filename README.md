<!--
SPDX-FileCopyrightText: 2021 Chris Saunders <chris@christophersaunders.ca>

SPDX-License-Identifier: GPL-3.0-or-later
-->

# lighterfluid

LighterFluid is a module for Foundry VTT that is designed to make it easier to assign lighting configuration to various tokens within your games. This tool is
system-agnostic and does not interface with items within a characters inventory the way some other lighting modules may. The goal of this module is to interface
with the innards of Foundry and various RPG systems as little as possible to reduce the risk that this module would suffer breakages as a result of changing APIs.

## Installation

You can install the nightly builds of LighterFluid onto your instance of Foundry by [pasting in the following manifest file.](https://gitlab.com/jiblits/lighterfluid/-/raw/trunk/src/module.json)

Once the module has made it closer to feature-completeness, the manifest will point to less volatile versions.

## Usage

### Configuration

![](https://gitlab.com/jiblits/lighterfluid/-/raw/trunk/documentation/assets/configuration-0.1.1-alpha.png)

Lights can be configured by clicking on the "Lighting Presets" button that will be made available in the "Lighting Controls" GM sidebar. Lights
can then be added or removed from the list.

**Light Configuration**

Lights need to have the following values:
- Name - This can be anything meaningful to you as a GM
- Colour - an RGB hex value for what you'd like the light to be. Use a [light picker tool](https://htmlcolorcodes.com/) or follow the [Foundry lighting style guidelines](https://foundryvtt.com/article/content-creation-guide/#lighting)
- Alpha - a decimal value between 0.0 and 1.0 which represents the intensity of the light. Using a value of 0.35 is a good default that doesn't wash out everything around it too much
- Bright Radius - The radius for which the light will be brightest. For systems like D&D5e this is the bright light from something like a torch.
- Dim Radius - The radius for which the light will continue to emit but duller. For systems like D&D5e this is the dim light from something like a torch.

It is reccomended to have a reset setting to simplify returning tokens to a known-default state (such as a light source named "Reset" or "None")

**Remember to click the "Save & Close" button once you have completed changing your settings**

### Assigning Light Sources

![](https://gitlab.com/jiblits/lighterfluid/-/raw/trunk/documentation/assets/token-light-picker-0.1.1-alpha.gif)

- To change the light settings on a token, right-click on it to bring up the token HUD. To the right of the HUD you should see the light sources you have set up for your game.
- Click on the light source you want to apply to the token.
- The light around the token should change immediately as a result.

## Feature Requests & Reporting Issues

Please submit feature requests and issues using the [issue tracker on Gitlab](https://gitlab.com/jiblits/lighterfluid/-/issues). When submitting issues please include
screenshots or screen recordings to help explain the issues you are experiencing.

Feature requests need to be limited in scope since this module is not intended to integrate into the numerous gaming systems for things such as inventory management.

# Development

## Prerequisites

In order to build this module, recent versions of `node` and `npm` are
required. Most likely using `yarn` also works but only `npm` is officially
supported. We recommend using the latest lts version of `node`, which is
`v14.15.5` at the time of writing. If you use `nvm` to manage your `node`
versions, you can simply run

```
nvm install
```

in the project's root directory.

You also need to install the the project's dependencies. To do so, run

```
npm install
```

## Building

You can build the project by running

```
npm run build
```

Alternatively, you can run

```
npm run build:watch
```

to watch for changes and automatically build as necessary.

## Linking the built project to Foundry VTT

In order to provide a fluent development experience, it is recommended to link
the built module to your local Foundry VTT installation's data folder. In
order to do so, first add a file called `foundryconfig.json` to the project root
with the following content:

```
{
  "dataPath": "/absolute/path/to/your/FoundryVTT/Data"
}
```

(if you are using Windows, make sure to use `\` as a path separator instead of
`/`)

Then run

```
npm run link-project
```

On Windows, creating symlinks requires administrator privileges so unfortunately
you need to run the above command in an administrator terminal for it to work.

## Running the tests

You can run the tests with the following command:

```
npm test
```
