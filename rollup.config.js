// SPDX-FileCopyrightText: 2021 Chris Saunders <chris@christophersaunders.ca>
//
// SPDX-License-Identifier: CC0-1.0

const { nodeResolve } = require('@rollup/plugin-node-resolve');

module.exports = {
  input: 'src/module/lighterfluid.js',
  output: {
    dir: 'dist/module',
    format: 'es',
    sourcemap: true,
  },
  plugins: [nodeResolve()],
};
