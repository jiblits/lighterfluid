// SPDX-FileCopyrightText: 2021 Chris Saunders <chris@christophersaunders.ca>
//
// SPDX-License-Identifier: GPL-3.0-or-later

describe('An example test', () => {
  it('will always succeed', () => {
    expect(true).toBeTruthy();
  });
});

// Add real tests here.
