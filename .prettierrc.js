// SPDX-FileCopyrightText: 2021 Chris Saunders <chris@christophersaunders.ca>
//
// SPDX-License-Identifier: CC0-1.0


module.exports = {
  semi: true,
  trailingComma: 'all',
  singleQuote: true,
  printWidth: 120,
  tabWidth: 2,
};
